# BOXINATOR

Boxinator is a [react](https://reactjs.org) application that allows you to make shipments across the world from your
computer or cellphone.
<br><br>

## INSTALLATION

The packet manager is needed to install the application

    npm install

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

```
npm create-react-app boxinator
```

## Available Scripts

### Start application

To start the application run the script:

```
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

For keycloak to work on your local computer you need to configure the keycloak.json. Change the resource to "local-client".

```
"resource": "local-client",
```

The page will reload if you make edits.\
You will also see any lint errors in the console.

###  

```
npm test
```

Launches the test runner in the interactive watch mode.\

###  

```
npm run build
```

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\

# DEPLOYMENT

This app is deployed on Heroku.

[https://boxinatorclient.herokuapp.com](https://boxinatorclient.herokuapp.com/login)

## CONTRIBUTORS

#### [Kristian Garberg](https://gitlab.com/Sirtakin)

#### [Alan Stér Helmersen](https://gitlab.com/alanshh)

#### [Håkon Kjelseth](https://gitlab.com/hakjelseth)

#### [Asso Rostampoor](https://gitlab.com/assoros)
