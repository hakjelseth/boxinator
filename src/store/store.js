import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {countriesMiddleware} from "./Middleware/countriesMiddleware";
import {countriesReducer} from "./Reducers/countriesReducer";
import {shipmentsReducer} from "./Reducers/shipmentsReducer"
import {shipmentsMiddleware} from "./Middleware/shipmentsMiddleware";
import {profileMiddleware} from "./Middleware/profileMiddleware";
import {profileReducer} from "./Reducers/profileReducer";
import {completeShipmentsMiddleware} from "./Middleware/completeShipmentsMiddleware";
import {completeShipmentsReducer} from "./Reducers/completeShipmentsReducer";
import {guestReducer} from "./Reducers/guestReducer";
import {guestMiddleware} from "./Middleware/guestMiddleware";

const rootReducers = combineReducers({
    countries: countriesReducer,
    shipments: shipmentsReducer,
    profile: profileReducer,
    completeShipments: completeShipmentsReducer,
    guest: guestReducer
})

const store = createStore(rootReducers, composeWithDevTools(
    applyMiddleware(countriesMiddleware),
    applyMiddleware(shipmentsMiddleware),
    applyMiddleware(profileMiddleware),
    applyMiddleware(completeShipmentsMiddleware),
    applyMiddleware(guestMiddleware)
))

export default store
