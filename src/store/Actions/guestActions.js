// TYPES
export const ACTION_GUEST_MAKE = '[guest] MAKE'
export const ACTION_GUEST_SET = '[guest] SET'
export const ACTION_GUEST_ORDER = '[guest] ORDER'

// SET
export const actionGuestSet = payload => ({
    type: ACTION_GUEST_SET,
    payload
})

export const actionGuestMake = payload => ({
    type: ACTION_GUEST_MAKE,
    payload
})

export const actionGuestOrder = payload => ({
    type: ACTION_GUEST_ORDER,
    payload
})
