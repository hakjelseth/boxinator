// TYPES
export const ACTION_SHIPMENTS_SET = '[shipments] SET';
export const ACTION_SHIPMENTS_FETCH_ALL = '[shipments] FETCH_ALL';
export const ACTION_SHIPMENTS_POST = '[shipments] POST';
export const ACTION_SHIPMENTS_DELETE = '[shipments] DELETE';
export const ACTION_SHIPMENTS_UPDATE = '[shipments] UPDATE';

// SET
export const actionShipmentsSet = payload => ({
    type: ACTION_SHIPMENTS_SET,
    payload
})


// FETCH
export const actionShipmentsFetchAll = () => ({
    type: ACTION_SHIPMENTS_FETCH_ALL
})


// POST
export const actionShipmentsPost = payload => ({
    type: ACTION_SHIPMENTS_POST,
    payload
})

// DELETE
export const actionShipmentsDelete = payload => ({
    type: ACTION_SHIPMENTS_DELETE,
    payload
})

// UPDATE
export const actionShipmentsUpdate = payload => ({
    type: ACTION_SHIPMENTS_UPDATE,
    payload
})