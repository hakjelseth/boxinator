// TYPES
export const ACTION_COMPLETE_SHIPMENTS_SET = '[completeShipments] COMPLETE_SET';
export const ACTION_COMPLETE_SHIPMENTS_FETCH_ALL = '[completeShipments] COMPLETE_FETCH_ALL';


export const actionCompleteShipmentsSet = payload => ({
    type: ACTION_COMPLETE_SHIPMENTS_SET,
    payload
})


export const actionCompleteShipmentsFetchAll = () => ({
    type: ACTION_COMPLETE_SHIPMENTS_FETCH_ALL
})

