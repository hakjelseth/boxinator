// TYPES
export const ACTION_COUNTRIES_SET = '[countries] SET';
export const ACTION_COUNTRIES_FETCH_ALL = '[countries] FETCH_ALL';
export const ACTION_COUNTRIES_ADD = '[countries] ADD';
export const ACTION_COUNTRIES_UPDATE = '[countries] UPDATE';
// SET
export const actionCountriesSet = payload => ({
    type: ACTION_COUNTRIES_SET,
    payload
})

// FETCH
export const actionCountriesFetchAll = () => ({
    type: ACTION_COUNTRIES_FETCH_ALL
})

// POST
export const actionCountriesAdd = payload => ({
    type: ACTION_COUNTRIES_ADD,
    payload
})

// PUT
export const actionCountriesUpdate = payload => ({
    type: ACTION_COUNTRIES_UPDATE,
    payload
})

