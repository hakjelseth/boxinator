// TYPES
export const ACTION_PROFILE_SET = '[profile] SET';
export const ACTION_PROFILE_FETCH = '[profile] FETCH_ALL';
export const ACTION_PROFILE_ERROR = '[profile] ERROR'
export const ACTION_PROFILE_UPDATE = '[profile] UPDATE'


// SET
export const actionProfileSet = payload => ({
    type: ACTION_PROFILE_SET,
    payload
})

// FETCH
export const actionProfileFetch = () => ({
    type: ACTION_PROFILE_FETCH
})

// UPDATE
export const actionProfileUpdate = payload => ({
    type: ACTION_PROFILE_UPDATE,
    payload
})

// ERROR
export const actionProfileError = payload => ({
    type: ACTION_PROFILE_ERROR,
    payload
})
