import {
    ACTION_SHIPMENTS_DELETE,
    ACTION_SHIPMENTS_FETCH_ALL,
    ACTION_SHIPMENTS_POST,
    ACTION_SHIPMENTS_SET,
    ACTION_SHIPMENTS_UPDATE
} from "../Actions/shipmentsActions";

const initialState = {
    shipments: [],
    completeShipments: [],
    fetching: false,
    error: ''
}

export const shipmentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_SHIPMENTS_SET:
            return {
                error: '',
                shipments: action.payload,
                fetching: false
            }


        case ACTION_SHIPMENTS_FETCH_ALL:
            return {
                fetching: true,
                ...state,
                error: ''
            }

        case ACTION_SHIPMENTS_POST:
            return {
                ...state,
                error: ''
            }

        case ACTION_SHIPMENTS_DELETE:
            return {
                ...state,
                error: ''
            }

        case ACTION_SHIPMENTS_UPDATE:
            return {
                ...state,
                error: ''
            }

        default:
            return state;
    }
}