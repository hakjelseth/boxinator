import {ACTION_COMPLETE_SHIPMENTS_FETCH_ALL, ACTION_COMPLETE_SHIPMENTS_SET} from "../Actions/completeShipmentsActions";

const initialState = {
    completeShipments: [],
    completeShipmentsFetching: false,
    error: ''
}

export const completeShipmentsReducer = (state = initialState, action) => {
    switch (action.type) {


        case ACTION_COMPLETE_SHIPMENTS_SET:
            return {
                error: '',
                completeShipments: action.payload,
                fetching: false
            }


        case ACTION_COMPLETE_SHIPMENTS_FETCH_ALL:
            return {
                fetching: true,
                ...state,
                error: ''
            }


        default:
            return state;
    }
}