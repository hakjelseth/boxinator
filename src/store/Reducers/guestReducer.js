import {ACTION_GUEST_MAKE, ACTION_GUEST_ORDER, ACTION_GUEST_SET} from "../Actions/guestActions";

const initialState = {
    guest: [],
    guestError: ''
}

export const guestReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_GUEST_SET:
            return {
                guestError: '',
                guest: action.payload
            }

        case ACTION_GUEST_MAKE:
            return {
                ...state,
                guestError: ''
            }

        case ACTION_GUEST_ORDER:
            return {
                ...state,
                guestError: ''
            }

        default:
            return state;
    }
}