import {
    ACTION_COUNTRIES_ADD,
    ACTION_COUNTRIES_FETCH_ALL,
    ACTION_COUNTRIES_SET,
    ACTION_COUNTRIES_UPDATE
} from "../Actions/countriesActions";

const initialState = {
    countries: [],
    error: ''
}

export const countriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_COUNTRIES_SET:
            return {
                error: '',
                countries: action.payload
            }

        case ACTION_COUNTRIES_FETCH_ALL:
            return {
                ...state,
                error: ''
            }

        case ACTION_COUNTRIES_ADD:
            return {
                ...state,
                error: ''
            }

        case ACTION_COUNTRIES_UPDATE:
            return {
                ...state,
                error: ''
            }

        default:
            return state;
    }
}