import {
    ACTION_PROFILE_ERROR,
    ACTION_PROFILE_FETCH,
    ACTION_PROFILE_SET,
    ACTION_PROFILE_UPDATE
} from "../Actions/profileActions";

const initialState = {
    profile: [],
    profileError: ''
}

export const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_PROFILE_SET:
            return {
                profileError: '',
                profile: action.payload
            }

        case ACTION_PROFILE_FETCH:
            return {
                ...state,
                profileError: ''
            }

        case ACTION_PROFILE_ERROR:
            return {
                ...state,
                profileError: action.payload
            }

        case ACTION_PROFILE_UPDATE:
            return {
                error: '',
                ...state
            }

        default:
            return state;
    }
}