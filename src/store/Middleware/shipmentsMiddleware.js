import KeycloakService from "../../services/KeycloakService";
import {
    ACTION_SHIPMENTS_DELETE,
    ACTION_SHIPMENTS_FETCH_ALL,
    ACTION_SHIPMENTS_POST,
    ACTION_SHIPMENTS_UPDATE,
    actionShipmentsFetchAll,
    actionShipmentsSet
} from "../Actions/shipmentsActions";
import {ShipmentsAPI} from "../../API/ShipmentsAPI";
import {trackPromise} from 'react-promise-tracker';

export const shipmentsMiddleware = ({dispatch}) => next => action => {

    next(action)

    if (action.type === ACTION_SHIPMENTS_FETCH_ALL) {
        trackPromise(
            ShipmentsAPI.getAccountShipments(KeycloakService.getToken())
                .then(shipments => {
                    dispatch(actionShipmentsSet(shipments))
                })
                .catch(error => {
                    dispatch(actionShipmentsSet([]))
                }));
    }


    if (action.type === ACTION_SHIPMENTS_POST) {
        ShipmentsAPI.orderPackage(action.payload)
            .then(shipments => {
                dispatch(actionShipmentsFetchAll())
            })
    }

    if (action.type === ACTION_SHIPMENTS_DELETE) {
        ShipmentsAPI.deleteShipment(action.payload)
            .then(shipments => {
                //dispatch(actionShipmentsFetchAll())
            })
    }

    if (action.type === ACTION_SHIPMENTS_UPDATE) {
        ShipmentsAPI.updateShipment(action.payload)
            .then(shipments => {
                dispatch(actionShipmentsFetchAll())
            })
    }
}