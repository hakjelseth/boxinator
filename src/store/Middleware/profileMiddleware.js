import {AccountAPI} from "../../API/AccountAPI";
import {
    ACTION_PROFILE_FETCH,
    ACTION_PROFILE_UPDATE,
    actionProfileError,
    actionProfileFetch,
    actionProfileSet
} from "../Actions/profileActions";
import KeycloakService from "../../services/KeycloakService";

export const profileMiddleware = ({dispatch}) => next => action => {

    next(action)

    if (action.type === ACTION_PROFILE_FETCH) {
        AccountAPI.login(KeycloakService.getToken())
            .then(profile => {
                dispatch(actionProfileSet(profile))
            })
            .catch(error => {
                dispatch(actionProfileSet([]))
            })
    }

    if (action.type === ACTION_PROFILE_UPDATE) {
        console.log("updateAPi", action.payload);
        AccountAPI.updateUser(action.payload)
            .then(result => {
                dispatch(actionProfileFetch())
            })
            .catch(error => {
                dispatch(actionProfileError(error.message))
            })
    }

}