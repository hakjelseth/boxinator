import KeycloakService from "../../services/KeycloakService";
import {ShipmentsAPI} from "../../API/ShipmentsAPI";
import {ACTION_COMPLETE_SHIPMENTS_FETCH_ALL, actionCompleteShipmentsSet} from "../Actions/completeShipmentsActions";
import {trackPromise} from 'react-promise-tracker';

export const completeShipmentsMiddleware = ({dispatch}) => next => action => {

    next(action)


    if (action.type === ACTION_COMPLETE_SHIPMENTS_FETCH_ALL) {
        trackPromise(
            ShipmentsAPI.getCompleteAccountShipments(KeycloakService.getToken())
                .then(shipments => {
                    dispatch(actionCompleteShipmentsSet(shipments))
                })
                .catch(error => {
                    dispatch(actionCompleteShipmentsSet([]))
                }));
    }


}