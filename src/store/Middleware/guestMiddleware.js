import {AccountAPI} from "../../API/AccountAPI";
import {ACTION_GUEST_MAKE, ACTION_GUEST_ORDER, actionGuestSet} from "../Actions/guestActions";
import {ShipmentsAPI} from "../../API/ShipmentsAPI";

export const guestMiddleware = ({dispatch}) => next => action => {

    next(action)

    if (action.type === ACTION_GUEST_MAKE) {
        AccountAPI.makeGuestUser(action.payload)
            .then(profile => {
                //console.log(profile)
                dispatch(actionGuestSet(profile));
            })
            .catch(error => {
                console.log(error)
            })
    }

    if (action.type === ACTION_GUEST_ORDER) {
        ShipmentsAPI.orderGuestPackage(action.payload)
            .then(profile => {
                //console.log(profile)
            })
            .catch(error => {
                console.log(error)
            })
    }
}