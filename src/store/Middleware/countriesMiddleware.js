import {
    ACTION_COUNTRIES_ADD,
    ACTION_COUNTRIES_FETCH_ALL,
    ACTION_COUNTRIES_UPDATE,
    actionCountriesFetchAll,
    actionCountriesSet
} from "../Actions/countriesActions";
import {CountriesAPI} from "../../API/CountriesAPI";
import KeycloakService from "../../services/KeycloakService";

export const countriesMiddleware = ({dispatch}) => next => action => {

    next(action)
    if (action.type === ACTION_COUNTRIES_FETCH_ALL) {
        CountriesAPI.getCountries(KeycloakService.getToken())
            .then(countries => {
                dispatch(actionCountriesSet(countries))
            })
            .catch(error => {
                dispatch(actionCountriesSet([]))
            })
    }

    if (action.type === ACTION_COUNTRIES_ADD) {
        CountriesAPI.addCountry(action.payload)
            .then(countries => {
                dispatch(actionCountriesFetchAll())
            })
            .catch(error => {
                console.log(error);
            })
    }

    if (action.type === ACTION_COUNTRIES_UPDATE) {
        CountriesAPI.updateCountry(action.payload)
            .then(countries => {
                dispatch(actionCountriesFetchAll())
            })
            .catch(error => {
                console.log(error);
            })
    }
}