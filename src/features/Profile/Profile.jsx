import withKeycloak from "../../hoc/withKeycloak"
import KeycloakService from "../../services/KeycloakService"
import React, { Fragment, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { actionProfileFetch, actionProfileUpdate } from "../../store/Actions/profileActions";
import { countryList } from "./country_list";
const SaveBtn = React.forwardRef((props, ref) => {
    return <button ref={ref} type="submit" disabled className="btn btn-dark mt-2">
        Save profile
    </button>
})
//Component that returns page of user attributes and functionality to edit them
const Profile = () => {
    const dispatch = useDispatch()
    //Button to save changes to user attributes
    const saveBtn = useRef(null)
    
    const { profile } = useSelector(state => state.profile)
    const [showInfo, setShowInfo] = useState(false)
    const [showError, setShowError] = useState(false)


    //Keep state of whether edit button to each attribute is clicked or not.
    const [showButton, setShowButton] = useState({
        "firstName": false,
        "lastName": false,
        "dateOfBirth": false,
        "countryOfResidence": false,
        "postalCode": false,
        "contactNumber": false
    })
    //Keep state of input values to determine user feedback
    const [inputValues, setInputValues] = useState({
        "firstName": '',
        "lastName": '',
        "contactNumber": '',
        "postalCode": '',
        "email": '',
        "dateOfBirth": '',
        "countryOfResidence": ''

    })
    //Fetch keycloak username and JWT token
    const username = KeycloakService.getUsername()
    const token = KeycloakService.getToken()

    //Fetch profile information and save button
    useEffect(() => {
        try {
            if (profile.length === 0)
                dispatch(actionProfileFetch())
        } catch (e) {}
    }, []);

    //Run when input values change
    useEffect(() => {
        //If all fields are empty, remove all user feedback and disable button
        if (
            inputValues["firstName"] === "" &&
            inputValues["lastName"] === "" &&
            inputValues["contactNumber"] === "" &&
            inputValues["postalCode"] === "" &&
            inputValues["email"] === "" &&
            inputValues["dateOfBirth"] === "" &&
            inputValues["countryOfResidence"] === ""
        ) {
            setShowError(false)
            setShowInfo(false)
            saveBtn.current.disabled = true
        }
        //Give error when non-numeric values are entered into numeric fields 
        if ((isNaN(inputValues["contactNumber"]) && inputValues["contactNumber"]) ||
            (isNaN(inputValues["postalCode"]) && inputValues["postalCode"])) {
            setShowError(true)
            setShowInfo(false)
        } else {
            setShowError(false)
        }


    }, [inputValues]);



    /**
    * Enable button to save user information and set input value to state and 
    */
    const onInputChange = e => {

        saveBtn.current.disabled = false;

        setInputValues({
            ...inputValues,
            [e.target.name]: e.target.value
        })
    }

    //Return new input value if non-empty, otherwise the old attribute
    const getNonEmptyAttr = (newAttribute, oldAttribute) => {
        if (newAttribute) {
            return newAttribute
        }
        return oldAttribute
    }

    //Save user attributes
    const submitChanges = (evt) => {
        saveBtn.current.disabled = true;
        evt.preventDefault();
        

        //Set error message if numeric fields are attempting to save non-numeric values 
        setShowError(false)
        if (isNaN(evt.target.postalCode.value) || isNaN(evt.target.contactNumber.value))
            setShowError(true)

        //Get value from input field or profile if input is empty
        const firstName = getNonEmptyAttr(evt.target.firstName.value, profile.firstName)
        const lastName = getNonEmptyAttr(evt.target.lastName.value, profile.lastName)
        const dateOfBirth = getNonEmptyAttr(evt.target.dateOfBirth.value, profile.dateOfBirth)
        const countryOfResidence = getNonEmptyAttr(evt.target.countryOfResidence.value, profile.countryOfResidence)
        const postalCode = getNonEmptyAttr(evt.target.postalCode.value, profile.postalCode)
        const contactNumber = getNonEmptyAttr(evt.target.contactNumber.value, profile.contactNumber)

        //Reset input fields
        evt.target.firstName.value = ""
        evt.target.lastName.value = ""
        evt.target.dateOfBirth.value = ""
        evt.target.countryOfResidence.value = ""
        evt.target.postalCode.value = ""
        evt.target.contactNumber.value = ""
        const accountId = profile.id
        const email = profile.email

        //Update profile API with all attributes
        dispatch(actionProfileUpdate({ accountId, email, firstName, lastName, dateOfBirth, countryOfResidence, postalCode, contactNumber, token }))
        dispatch(actionProfileFetch())

        //Successful user feedback
        setShowInfo(true)
    }

    //Toggle edit state of given attribute  
    const editAttribute = (evt) => {
        evt.preventDefault()
        setShowButton(showButton => ({
            ...showButton,
            [evt.target.value]: !showButton[evt.target.value]
        }))

    }

    //camel case string -> capitalized words with spaces string
    const convertCamelCase = (string) => {
        return string.replace(/([A-Z])/g, " $1").replace(/^./, function (str) {
            return str.toUpperCase();
        });
    };

    /**
    * Return one row of a given attribute
    * Includes title, value, edit button and input field
    * @param {string} name
    * @param {string | number} attr
    * @param {boolean} disabled
    * @returns JSX.element
    */
    const inputAttribute = (name, attr, disabled) => {
        if(name === "id")
            return null
        return (
            <div className="row mb-3">
                <label className="col-md-2 offset-md-0">
                    <strong>{convertCamelCase(name)}</strong>
                </label>
                <div id={name} className="col-md-3">

                    {/* If disabled, only return attribute value.
                        Otherwise, attribute is editable*/}
                    {disabled ? (
                        attr
                    ) : (

                        <div className="col">
                            <div className="row mb-3">
                                <p className="col-8">{name === "dateOfBirth" ? attr.split("T")[0] : attr}</p>
                                {/* Edit button to toggle display state of input field */}
                                <button type="button"
                                    className="input-group-text col col-sm-auto btn btn-sm btn-link btn-light"
                                    onClick={editAttribute} value={name}>
                                    <div className="pe-none me-2">
                                        <span className="btn-sm material-icons ">
                                            edit
                                        </span>
                                        Edit
                                    </div>
                                </button>
                            </div>
                            {/* Input fields 
                                    for country, fetch a list of countries into select elemeent
                                    otherwise, set an input field*/}
                            {name === "countryOfResidence" ?
                                <select
                                    onChange={onInputChange}
                                    name={"countryOfResidence"}
                                    style={showButton[name] ? {} : { display: 'none' }}
                                    className="form-select mb-3"
                                >
                                    {countryList()}
                                </select>
                                :
                                <input
                                    onChange={onInputChange}
                                    name={name}
                                    style={showButton[name] ? {} : { display: 'none' }}
                                    className="form-control mb-3"
                                    type={name === "dateOfBirth" ? "date" : "text"}
                                    placeholder={"New " + convertCamelCase(name).toLowerCase()}
                                />
                            }

                        </div>
                    )}
                </div>
            </div>
        );
    };


    return (
        <div className="container border p-3 my-3">
            <main>
                <h1 className="mb-3">Welcome to your profile, {username}</h1>
                <form id="editData" onSubmit={submitChanges}>
                    {/* Traverse profile state of attributes and return rows of information and input fields */}
                    <ul>
                        {profile ? Object.keys(profile)?.map((key, index) => (
                            <div key={index}>
                                {inputAttribute(
                                    key,
                                    profile[key],
                                    //Return true for email and id so that input fields are disabled
                                    ["email", "id"].includes(key)
                                )}
                            </div>)) : <h3>Loading profile...</h3>
                        }
                    </ul>
                    {/* Give user feedback */}
                    {showInfo && !showError && <div id="info" className="alert alert-primary" role="alert">
                        Your information was saved
                    </div>}
                    {showError && <div className="alert alert-danger" role="alert">
                        Invalid input
                    </div>}
                    {/* Run submitChanges function when clicked */}
                    <SaveBtn ref={saveBtn}></SaveBtn>
                </form>
            </main>
        </div>
    );
};
export default withKeycloak(Profile);
