import {Redirect, useHistory} from "react-router-dom"
import KeycloakService from "../../services/KeycloakService"

const Login = () => {
    const history = useHistory()
    if (KeycloakService.isLoggedIn()) {
        return <Redirect to="/dashboard"/>
    }

    // Go to keycloak login
    const handleLoginClick = () => {
        KeycloakService.doLogin()
    }

    // Go to guest page
    const handleLaterClick = () => {
        history.push("/guest");
    }

    return (
        <div className="container border p-3 my-3">
            <main className="d-flex flex-column align-items-center align-items-xl-center">
                <h1>Welcome to the Boxinator</h1>
                <img src="/front.png" alt="image"/>
                <p>Ship a mystery box right to your door!</p>
                <button className="btn btn-dark mt-2" onClick={handleLoginClick}>Login to Boxinator</button>
                <button className="btn btn-dark mt-2" onClick={handleLaterClick}>Register later</button>
            </main>
        </div>
    )
}
export default Login