import KeycloakService from "../../services/KeycloakService"
import React, {useEffect, useRef} from "react";
import Modal from 'react-modal';
import {useDispatch, useSelector} from "react-redux";
import {actionCountriesFetchAll} from "../../store/Actions/countriesActions";
import {actionGuestMake, actionGuestOrder} from "../../store/Actions/guestActions";

const customStyles = {
    content: {
        background: 'cornflowerblue',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        margin: '0 3px',
    },
};

const Guest = () => {
    const firstUpdate = useRef(true);
    const dispatch = useDispatch()
    const {countries} = useSelector(state => state.countries)
    const {guest} = useSelector(state => state.guest)
    const [errorMessage, setErrorMessage] = React.useState("");
    const [shipment, setShipment] = React.useState({});
    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);
    Modal.setAppElement('#root');


    function openModal() {
        setIsOpen(true);
    }

    function afterOpenModal() {
        // references are now synced and can be accessed.
        subtitle.style.color = '#ffffff';
    }

    function closeModal() {
        setIsOpen(false);
    }

    // Get info about countries
    useEffect(() => {
        if (countries.length === 0)
            dispatch(actionCountriesFetchAll())
    }, []);

    useEffect(() => {
        if (firstUpdate.current) {
            firstUpdate.current = false;
            return;
        }
        if (guest.status !== 400) {
            setErrorMessage("Successfully ordered package");
            dispatch(actionGuestOrder(shipment));
        } else {
            setErrorMessage("A user with that email already exists.");
        }
    }, [guest]);

    // Make guest shipment
    const order = (evt) => {
        evt.preventDefault();
        const email = evt.target.email.value;
        const receiverName = evt.target.recName.value
        const weight = evt.target.weight.value
        const boxColor = evt.target.color.value
        const countryName = evt.target.country.value
        const regUrl = KeycloakService.createRegistration();
        setShipment({email, receiverName, weight, boxColor, countryName, regUrl})
        dispatch(actionGuestMake(email));

        closeModal();
    }

    return (
        <div className="container border p-3 my-3">
            <main>
                <h3>Guest orders</h3>
                <p>Here you can order a package without registration. You will receive a receipt in the specified email
                    account.</p>
                {errorMessage ? <div className="alert alert-primary" role="alert">{errorMessage}</div> : ""}
                <button type="button" className="btn btn-primary btn-lg" onClick={openModal}>Create New Shipment
                </button>
                <div>
                    <Modal
                        isOpen={modalIsOpen}
                        onAfterOpen={afterOpenModal}
                        onRequestClose={closeModal}
                        style={customStyles}
                        contentLabel="Example Modal"
                    >
                        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Send new package</h2>

                        <div>Enter details for shipment</div>
                        <form onSubmit={order} className="orderForm">
                            <input required type="email" className="form-control" name="email"
                                   placeholder="Enter your email"/><br/>
                            <input required name="recName" className="form-control" placeholder=" Enter receiver name"/><br/>
                            <select name="weight" className="form-control" defaultValue={"Weight of package"}>
                                <option value="Basic" name="Basic">Basic</option>
                                <option value="Humble" name="Humble">Humble</option>
                                <option value="Deluxe" name="Deluxe">Deluxe</option>
                                <option value="Premium" name="Premium">Premium</option>
                            </select><br/>
                            <label>Box color:</label>
                            <input type="color" className="form-control form-control-color" name="color"
                                   placeholder="Color"/><br/>
                            <select className="form-select" name="country">
                                {countries?.map((country, index) => <option value={country.name} key={index}>
                                    {country.name}
                                </option>)}
                            </select><br/>
                            <button type="submit" className="btn btn-success" style={{marginRight: "5px"}}>Send</button>
                            <button className="btn btn-danger" onClick={closeModal}>Close</button>
                        </form>
                    </Modal>
                </div>
            </main>
        </div>

    )
}
export default Guest