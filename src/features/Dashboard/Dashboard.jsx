import withKeycloak from "../../hoc/withKeycloak"
import KeycloakService from "../../services/KeycloakService"
import React, { useEffect } from "react";
import Modal from 'react-modal';
import { useDispatch, useSelector } from "react-redux";
import { actionCountriesFetchAll } from "../../store/Actions/countriesActions";
import { actionShipmentsFetchAll, actionShipmentsPost } from "../../store/Actions/shipmentsActions";
import { Redirect } from "react-router-dom";
import { actionProfileFetch } from "../../store/Actions/profileActions";
import { actionCompleteShipmentsFetchAll } from "../../store/Actions/completeShipmentsActions";
import ColorPicker from "../../services/ColorPicker";
import LoadThreeDots from "../../services/LoadThreeDots";
import { usePromiseTracker } from "react-promise-tracker";

const customStyles = {
    content: {
        background: 'cornflowerblue',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        margin: '0 3px',
    },
};

const Dashboard = () => {
    const { countries } = useSelector(state => state.countries)
    const { shipments, fetching } = useSelector(state => state.shipments)
    const { completeShipments, completeShipmentsFetching } = useSelector(state => state.completeShipments)
    const { profile } = useSelector(state => state.profile)
    const { promiseInProgress } = usePromiseTracker();
    const username = KeycloakService.getUsername()
    const token = KeycloakService.getToken()
    const dispatch = useDispatch()
    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [infoModalIsOpen, setInfoIsOpen] = React.useState(false);
    const [currentIndex, setCurrentIndex] = React.useState(0);
    Modal.setAppElement('#root');

    function openModal() {
        setIsOpen(true);
    }

    function infoModal() {
        setInfoIsOpen(!infoModalIsOpen);
    }

    function afterOpenModal() {
        // references are now synced and can be accessed.
        subtitle.style.color = '#ffffff';
    }

    function closeModal() {
        setIsOpen(false);
    }

    // Get info from api
    useEffect(() => {
        try {
            if (profile.length === 0)
                dispatch(actionProfileFetch())
        } catch (e) {
        }
        try {
            if (countries.length === 0)
                dispatch(actionCountriesFetchAll())
        } catch (e) {
        }
    }, []);



    // Get info about shipments
    useEffect(() => {
        try {
            if (shipments.length === 0)
                dispatch(actionShipmentsFetchAll())
            if (completeShipments.length === 0)
                dispatch(actionCompleteShipmentsFetchAll())

        } catch (e) {
            console.log(e)
        }
    }, [profile]);


    if (KeycloakService.hasRole(["Administrator"])) {
        return <Redirect to="/admin" />
    }

    // Make new shipment
    const order = async (evt) => {
        evt.preventDefault();
        let recName = evt.target.recName.value
        let weight = evt.target.weight.value
        let color = evt.target.color.value
        let country = evt.target.country.value
        dispatch(actionShipmentsPost({ recName, weight, color, country, token }))
        closeModal();
    }

    return (
        <div className="container border p-3 my-3">
            <main>
                <div>
                    <h1 className="mb-3">Welcome to the Dashboard, {username}</h1>
                </div>
                <h4>Shipments under way</h4>
                {promiseInProgress ? (
                    <LoadThreeDots />
                ) : (
                    <div className="table-responsive">
                        <table className="table">
                            <tbody>
                                <tr>
                                    <th>Box nr</th>
                                    <th>Receiver</th>
                                    <th>Weight</th>
                                    <th>Box color</th>
                                    <th>Status</th>
                                    <th>Country</th>
                                    <th>Cost</th>
                                </tr>
                                {shipments.length > 0 ?(shipments ? shipments?.map((ship, index) => <tr onClick={() => {
                                    console.log(shipments);
                                    infoModal();
                                    setCurrentIndex(index)
                                }} key={index}>
                                    <td>{index + 1} </td>
                                    <td>{ship.receiverName} </td>
                                    <td>{ship.weight} </td>
                                    <td style={{ backgroundColor: ship.boxColor, color: ColorPicker.pickTextColorBasedOnBgColorAdvanced(ship.boxColor, '#FFFFFF', '#000000') }}>{ship.boxColor} </td>
                                    <td>{ship.status.split(",").at(-1)} </td>
                                    <td>{ship.countryName} </td>
                                    <td>{ship.cost}</td>
                                </tr>) : <h1>Loading shipments</h1>) : <tr style={{ backgroundColor: "#ffffff" }}><td colspan="100%"> You have no shipments on the way </td></tr>}
                            </tbody>
                        </table>
                    </div>)}

                <h4 className="mt-4">Completed shipments</h4>
                {promiseInProgress ? (
                    <LoadThreeDots />
                ) : (
                    <div className="table-responsive">
                        <table className="table">
                            <tbody>
                                <tr>
                                    <th>Box nr</th>
                                    <th>Receiver</th>
                                    <th>Weight</th>
                                    <th>Box color</th>
                                    <th>Status</th>
                                    <th>Country</th>
                                    <th>Cost</th>
                                </tr>
                                {completeShipments.length > 0 ? (completeShipments ? completeShipments?.map((ship, index) => <tr key={index}>
                                    <td>{index + 1} </td>
                                    <td>{ship.receiverName} </td>
                                    <td>{ship.weight} </td>
                                    <td style={{ backgroundColor: ship.boxColor, color: ColorPicker.pickTextColorBasedOnBgColorAdvanced(ship.boxColor, '#FFFFFF', '#000000') }}>{ship.boxColor} </td>
                                    <td>{ship.status.split(",").at(-1)} </td>
                                    <td>{ship.countryName} </td>
                                    <td>{ship.cost}</td>
                                </tr>) : <h1>Loading shipments</h1>) : <tr style={{ backgroundColor: "#ffffff" }}><td colspan="100%"> You have no completed shipments </td></tr>}
                            </tbody>
                        </table>
                    </div>)}

                <button type="button" className="mt-3 btn btn-primary btn-lg" onClick={openModal}>Create new shipment
                </button>
                <div>
                    <Modal
                        isOpen={modalIsOpen}
                        onAfterOpen={afterOpenModal}
                        onRequestClose={closeModal}
                        style={customStyles}
                        contentLabel="Order Modal"
                    >
                        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Send new package</h2>

                        <div>Enter details for shipment</div>
                        <form onSubmit={order} className="orderForm">
                            <input required name="recName" className="form-control" placeholder=" Enter receiver name" /><br />
                            <select name="weight" className="form-select" defaultValue={"Weight of package"}>
                                <option value="Basic" name="Basic">Basic</option>
                                <option value="Humble" name="Humble">Humble</option>
                                <option value="Deluxe" name="Deluxe">Deluxe</option>
                                <option value="Premium" name="Premium">Premium</option>
                            </select><br />
                            <label>Box color:</label>
                            <input type="color" className="form-control form-control-color" name="color"
                                placeholder="Color" /><br />
                            <select className="form-select" name="country">
                                {countries?.map((country, index) => <option value={country.name} key={index}>
                                    {country.name}
                                </option>)}
                            </select><br />
                            <button type="submit" className="btn btn-success" style={{ marginRight: "5px" }}>Send</button>
                            <button className="btn btn-danger" onClick={closeModal}>Close</button>
                        </form>
                    </Modal>
                </div>
                <div>
                    <Modal
                        isOpen={infoModalIsOpen}
                        onAfterOpen={afterOpenModal}
                        onRequestClose={infoModal}
                        style={customStyles}
                        contentLabel="Info Modal"
                    >
                        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Info for package</h2>
                        <div>
                            {shipments[currentIndex] ?
                                <div>
                                    <p>Receiver: {shipments[currentIndex].receiverName}</p>
                                    <p>Statuses: {shipments[currentIndex].status}</p>
                                    <p>Weight class: {shipments[currentIndex].weight}</p>
                                    <p>Destination: {shipments[currentIndex].countryName}</p>
                                </div>


                                : ""
                            }
                        </div>

                    </Modal>
                </div>
            </main>
        </div>

    )
}
export default withKeycloak(Dashboard)