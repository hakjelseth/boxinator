import React from 'react';
import {Redirect, Route} from "react-router-dom";
import KeycloakService from "../../services/KeycloakService";

const UserRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={(props) => (
        KeycloakService.isLoggedIn() === true
            ? <Component {...props} />
            : <Redirect to='/login'/>
    )}/>
)

export default UserRoute;