import withKeycloak from "../../hoc/withKeycloak"
import KeycloakService from "../../services/KeycloakService"
import React, {Fragment, useEffect, useState} from "react";
import ReadOnlyShipmentsRow from "../Rows/ReadOnlyShipmentsRow";
import EditableShipmentsRows from "../Rows/EditableShipmentsRows";
import ReadOnlyCountriesRow from "../Rows/ReadOnlyCountriesRow";
import {useDispatch, useSelector} from "react-redux";
import {
    actionCountriesAdd,
    actionCountriesFetchAll,
    actionCountriesSet,
    actionCountriesUpdate
} from "../../store/Actions/countriesActions";
import EditableCountriesRows from "../Rows/EditableCountriesRows";
import {
    actionShipmentsDelete,
    actionShipmentsFetchAll,
    actionShipmentsSet,
    actionShipmentsUpdate
} from "../../store/Actions/shipmentsActions";
import Modal from "react-modal";
import {actionProfileFetch} from "../../store/Actions/profileActions";
import {usePromiseTracker} from "react-promise-tracker";
import LoadThreeDots from "../../services/LoadThreeDots";

const customStyles = {
    content: {
        background: 'cornflowerblue',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

const Dashboard = () => {
    const dispatch = useDispatch()
    // Get state
    const {countries} = useSelector(state => state.countries)
    const {shipments} = useSelector(state => state.shipments)
    const {profile} = useSelector(state => state.profile)
    const {promiseInProgress} = usePromiseTracker();
    const username = KeycloakService.getUsername()
    const token = KeycloakService.getToken()
    const [editFormData, setEditFormData] = useState({
        receiverName: "",
        weight: "",
        boxColor: "",
        status: "",
        countryName: "",
        cost: 0
    });

    const [editShipmentId, setEditShipmentId] = useState(null);

    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);
    Modal.setAppElement('#root');

    function openModal() {
        setIsOpen(true);
    }

    function afterOpenModal() {
        // references are now synced and can be accessed.
        subtitle.style.color = '#ffffff';
    }

    function closeModal() {
        setIsOpen(false);
    }

    useEffect(() => {
        if (profile === {})
            dispatch(actionProfileFetch())
        if (countries.length === 0)
            dispatch(actionCountriesFetchAll())
        if (shipments.length === 0)
            dispatch(actionShipmentsFetchAll())
    }, []);

    //Change shipment to edit mode.
    const handleEditClick = (event, shipment) => {
        event.preventDefault();
        setEditShipmentId(shipment.id);
        const formValues = {
            accountID: shipment.accountID,
            receiverName: shipment.receiverName,
            weight: shipment.weight,
            boxColor: shipment.boxColor,
            status: shipment.status.split(",").at(-1),
            countryName: shipment.countryName,
            cost: shipment.cost
        };

        setEditFormData(formValues);
    };

    // Delete shipment
    const handleDeleteClick = (shipmentId) => {
        let confirm = window.confirm("Are you sure you want to delete?");
        if (confirm) {
            const newShipments = [...shipments];

            const index = shipments.findIndex((shipment) => shipment.id === shipmentId);

            dispatch(actionShipmentsDelete({token, shipmentId}));

            newShipments.splice(index, 1);

            dispatch(actionShipmentsSet(newShipments))
        }

    };

    // Cancel editing shipment
    const handleCancelClick = () => {
        setEditShipmentId(null);
    };

    // Handle changes in form
    const handleEditFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = {...editFormData};
        newFormData[fieldName] = fieldValue;

        setEditFormData(newFormData);
    };

    // Submit edited shipment to db.
    const handleEditFormSubmit = (event) => {
        event.preventDefault();
        const editedShipment = {
            id: editShipmentId,
            accountId: editFormData.accountID,
            receiverName: editFormData.receiverName,
            weight: editFormData.weight,
            boxColor: editFormData.boxColor,
            status: editFormData.status,
            countryName: editFormData.countryName,
            cost: editFormData.cost
        };

        const newShipments = [...shipments];

        const index = shipments.findIndex((shipment) => shipment.id === editShipmentId);

        dispatch(actionShipmentsUpdate({editedShipment, token}));

        newShipments[index] = editedShipment;

        setEditShipmentId(null);
    };


    const [editCountriesFormData, setCountriesEditFormData] = useState({
        name: "",
        multiplier: 0
    });

    const [editCountryId, setEditCountryId] = useState(null);

    // Set country to edit mode
    const handleCountriesEditClick = (event, country) => {
        event.preventDefault();
        setEditCountryId(country.id);

        const formValues = {
            name: country.name,
            multiplier: country.multiplier
        };

        setCountriesEditFormData(formValues);
    };

    // Cancel edit
    const handleCountriesCancelClick = () => {
        setEditCountryId(null);
    };

    // Handles changes when the form changes
    const handleCountriesEditFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = {...editCountriesFormData};
        newFormData[fieldName] = fieldValue;

        setCountriesEditFormData(newFormData);
    };

    // Submit the edited country and update db
    const handleCountriesEditFormSubmit = (event) => {
        event.preventDefault();

        const editedCountry = {
            name: editCountriesFormData.name,
            multiplier: editCountriesFormData.multiplier,
            id: editCountryId
        };

        const newCountries = [...countries];

        const index = countries.findIndex((country) => country.id === editCountryId);

        dispatch(actionCountriesUpdate({editedCountry, token}))

        newCountries[index] = editedCountry;

        dispatch(actionCountriesSet(newCountries));
        //dispatch(actionShipmentsFetchAll());
        setEditCountryId(null);
    };

    // Add new country to the db
    const addNewCountry = (evt) => {
        evt.preventDefault();
        let countryName = evt.target.countryName.value
        let multiplier = evt.target.multiplier.value
        dispatch(actionCountriesAdd({countryName, multiplier, token}))
        dispatch(actionCountriesFetchAll())
        closeModal();
    }


    return (
        <div className="container border p-3 my-3">
            <main>
                <div>
                    <h1>Welcome to the Admin page, {username}</h1>
                </div>
                <h3 className="mt-3">All shipments</h3>
                {promiseInProgress ? (
                    <LoadThreeDots/>
                ) : (
                    <form onSubmit={handleEditFormSubmit}>
                        <div className="table-responsive">
                            <table className="table">
                                <tbody>
                                <tr>
                                    <th>Box nr</th>
                                    <th>Sender</th>
                                    <th>Receiver</th>
                                    <th>Weight</th>
                                    <th>Box color</th>
                                    <th>Status</th>
                                    <th>Country</th>
                                    <th>Cost</th>
                                </tr>
                                {shipments?.map((shipment, index) =>
                                    <Fragment key={index}>
                                        {editShipmentId === shipment.id ? (
                                            <EditableShipmentsRows
                                                editFormData={editFormData}
                                                index={index}
                                                countries={countries}
                                                handleEditFormChange={handleEditFormChange}
                                                handleCancelClick={handleCancelClick}
                                            />
                                        ) : (
                                            <ReadOnlyShipmentsRow
                                                shipment={shipment}
                                                index={index}
                                                handleEditClick={handleEditClick}
                                                handleDeleteClick={handleDeleteClick}
                                            />
                                        )}
                                    </Fragment>
                                )}
                                </tbody>
                            </table>
                        </div>

                    </form>)}


                <h3 className="mt-4">All countries</h3>
                {promiseInProgress ? (
                    <LoadThreeDots/>
                ) : (
                    <form onSubmit={handleCountriesEditFormSubmit}>
                        <div className="table-responsive">
                        <table className="table">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Multiplier</th>
                            </tr>
                            {countries?.map((country, index) =>
                                <Fragment key={index}>
                                    {editCountryId === country.id ? (
                                        <EditableCountriesRows
                                            editFormData={editCountriesFormData}
                                            countries={countries}
                                            handleEditFormChange={handleCountriesEditFormChange}
                                            handleCancelClick={handleCountriesCancelClick}
                                        />
                                    ) : (
                                        <ReadOnlyCountriesRow
                                            country={country}
                                            handleEditClick={handleCountriesEditClick}
                                        />
                                    )}
                                </Fragment>
                            )}
                            </tbody>
                        </table>
                        </div>
                    </form>)}
                <button type="button" className="btn btn-primary btn-lg" onClick={openModal}>Add new country</button>
                <div>
                    <Modal
                        isOpen={modalIsOpen}
                        onAfterOpen={afterOpenModal}
                        onRequestClose={closeModal}
                        style={customStyles}
                        contentLabel="New country"
                    >
                        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Add new country</h2>

                        <div>Enter details for country</div>
                        <form onSubmit={addNewCountry} className="orderForm">
                            <input type="text" className="form-control" name="countryName"
                                   placeholder="Enter country name"/>
                            <input type="number" className="form-control" name="multiplier"
                                   placeholder="Enter multiplier"/>
                            <button type="submit" className="btn btn-dark mt-2" id="sendButton">Send</button>
                            <button className="btn btn-dark mt-2" id="closeButton" onClick={closeModal}>Close</button>
                        </form>
                    </Modal>
                </div>
            </main>
        </div>

    )
}
export default withKeycloak(Dashboard)