import KeycloakService from "../../services/KeycloakService";
import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";

const NavbarUserMenu = () => {
    const handleLogoutClick = () => {
        if (window.confirm("Are you sure?")) {
            KeycloakService.doLogout();
        }
    };
    const username = KeycloakService.getUsername()
    const rolePath = KeycloakService.hasRole(["Administrator"]) ? "/admin" : "/dashboard"

    return (
        <Navbar id="basic-navbar-nav">
            <Nav
                defaultActiveKey={rolePath}
                className="me-auto"
            >
                <Nav.Link as={Link} eventKey={rolePath} to={rolePath}>
                    Dashboard
                </Nav.Link>
                <Nav.Link as={Link} eventKey="/profile" to="/profile">
                    {username}
                </Nav.Link>
                <button
                    className="btn btn-secondary ms-lg-5 btn-xs"
                    onClick={handleLogoutClick}
                >
                    Logout
                </button>
            </Nav>
        </Navbar>
    );
};
export default NavbarUserMenu;
