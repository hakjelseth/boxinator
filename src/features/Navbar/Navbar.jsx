import {Container, Navbar} from "react-bootstrap";
import KeycloakService from "../../services/KeycloakService";
import NavbarUserMenu from './NavbarUserMenu'

const NavBar = () => {
    return (
        <Navbar expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href=".">Boxinator</Navbar.Brand>
                {KeycloakService.isLoggedIn() && <NavbarUserMenu/>}
            </Container>
        </Navbar>
    );
};

export default NavBar;
