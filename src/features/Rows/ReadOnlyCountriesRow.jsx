import React from "react";

const ReadOnlyCountriesRow = ({country, handleEditClick}) => {
    return (
        <tr>
            <td>{country.name} </td>
            <td>{country.multiplier} </td>

            <td>
                <button
                    className="btn btn-primary"
                    type="button"
                    onClick={(event) => handleEditClick(event, country)}
                >
                    Edit
                </button>
            </td>
        </tr>
    );
};

export default ReadOnlyCountriesRow;