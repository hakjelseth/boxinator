import React from "react";

const EditableShipmentsRows = ({
                                   editFormData,
                                   handleEditFormChange,
                                   handleCancelClick,
                               }) => {
    return (
        <tr>
            <td>
                {editFormData.name}
            </td>
            <td>
                <input
                    type="number"
                    required="required"
                    placeholder="Enter a number"
                    name="multiplier"
                    value={editFormData.multiplier}
                    onChange={handleEditFormChange}
                />
            </td>
            <td>
                <button className="btn btn-primary" style={{marginRight: "5px"}}
                        type="submit">Save
                </button>
                <button className="btn btn-primary" type="button" onClick={handleCancelClick}>
                    Cancel
                </button>
            </td>
        </tr>
    );
};

export default EditableShipmentsRows;