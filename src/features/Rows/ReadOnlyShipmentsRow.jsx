import React from "react";
import ColorPicker from "../../services/ColorPicker";

const ReadOnlyShipmentsRow = ({shipment, index, handleEditClick, handleDeleteClick}) => {
    const status = shipment.status.split(",");
    return (
        <tr>
            <td>{index + 1} </td>
            <td>{shipment.accountID}</td>
            <td>{shipment.receiverName} </td>
            <td>{shipment.weight} </td>
            <td style={{ backgroundColor: shipment.boxColor, color: ColorPicker.pickTextColorBasedOnBgColorAdvanced(shipment.boxColor, '#FFFFFF', '#000000') }}>{shipment.boxColor} </td>
            <td>{status[status.length-1]} </td>
            <td>{shipment.countryName} </td>
            <td>{shipment.cost}</td>
            <td>
                <button
                    className="btn btn-primary"
                    style={{marginRight: "5px"}}
                    type="button"
                    onClick={(event) => handleEditClick(event, shipment)}
                >
                    Edit
                </button>
                <button className="btn btn-primary" type="button" onClick={() => handleDeleteClick(shipment.id)}>
                    Delete
                </button>
            </td>
        </tr>
    );
};

export default ReadOnlyShipmentsRow;