import React from "react";

const EditableShipmentsRows = ({
                                   editFormData,
                                   index,
                                   countries,
                                   handleEditFormChange,
                                   handleCancelClick,
                               }) => {
    return (
        <tr>
            <td>
                {index + 1}
            </td>
            <td>
                {editFormData.accountID}
            </td>
            <td>
                <input
                    type="text"
                    required="required"
                    placeholder="Enter a name"
                    name="receiverName"
                    value={editFormData.receiverName}
                    onChange={handleEditFormChange}
                />
            </td>
            <td>
                <select name="weight" defaultValue={editFormData.weight} onChange={handleEditFormChange}>
                    <option value="Basic" name="Basic">Basic</option>
                    <option value="Humble" name="Humble">Humble</option>
                    <option value="Deluxe" name="Deluxe">Deluxe</option>
                    <option value="Premium" name="Premium">Premium</option>
                </select>
            </td>
            <td>
                <input
                    type="color"
                    required="required"
                    name="boxColor"
                    placeholder="Color"
                    value={editFormData.boxColor}
                    onChange={handleEditFormChange}
                />
            </td>
            <td>
                <select name="status" value={editFormData.status} onChange={handleEditFormChange}>
                    <option value="CREATED" name="CREATED">CREATED</option>
                    <option value="RECEIVED" name="RECEIVED">RECEIVED</option>
                    <option value="INTRANSIT" name="INTRANSIT">INTRANSIT</option>
                    <option value="COMPLETED" name="COMPLETED">COMPLETED</option>
                    <option value="CANCELLED" name="CANCELLED">CANCELLED</option>
                </select>
            </td>
            <td>
                <select name="countryName" value={editFormData.countryName} onChange={handleEditFormChange}>
                    {countries.map((country, index) => <option value={country.name} key={index}>
                        {country.name}
                    </option>)}
                </select>
            </td>
            <td>
                {editFormData.cost}
            </td>
            <td>
                <button className="btn btn-primary" style={{marginRight: "5px"}} type="submit">Save</button>
                <button className="btn btn-primary" type="button" onClick={handleCancelClick}>
                    Cancel
                </button>
            </td>
        </tr>
    );
};

export default EditableShipmentsRows;