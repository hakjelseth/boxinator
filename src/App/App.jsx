import "./App.css";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Dashboard from "../features/Dashboard/Dashboard";
import Login from "../features/Login/Login";
import NavBar from "../features/Navbar/Navbar";
import Profile from "../features/Profile/Profile";
import Admin from "../features/Admin/Admin";
import GuardedRoute from "../features/Guard/AdminRoute";
import UserRoute from "../features/Guard/UserRoute";
import LoggedInRoute from "../features/Guard/LoggedInRoute";
import Guest from "../features/Guest/Guest";
import {Scrollbars} from 'react-custom-scrollbars-2';


const App = () => {

    return (

        <BrowserRouter>
            <Scrollbars style={{height: `100vh`}}>
                <NavBar/>
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/login"/>
                    </Route>
                    
                    <LoggedInRoute path="/login" component={Login}/>
                    <UserRoute path="/dashboard" component={Dashboard}/>
                    <UserRoute path="/profile" component={Profile}/>
                    <GuardedRoute path="/admin" component={Admin}/>
                    <Route path="/guest" component={Guest}/>
                    <Redirect from="*" to="/" />
                </Switch>
            </Scrollbars>
        </BrowserRouter>

    )
}

export default App;
