const baseURL = 'https://boxinatorapi.azurewebsites.net'

export const AccountAPI = {
    async getAccountInfo(jwt) {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const user = await fetch(`${baseURL}/api/Accounts/account_id`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })

        console.log(user)
        return user
    },

    async login(jwt) {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`
            }
        }
        const user = await fetch(`${baseURL}/api/Accounts/Login`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                console.log(error);
            })
        return user
    },

    async makeGuestUser(email) {
        const requestOptions = {
            method: 'POST',
            body: JSON.stringify({
                "id": email,
                "email": email,
                "firstName": "guest",
                "lastName": "guest"
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const user = await fetch(`${baseURL}/api/Accounts/guest`, requestOptions)
            .then(response => response.json())
            .catch((error) => {
                throw error;
            })

        return user
    },

    async updateUser({
                         accountId,
                         email,
                         firstName,
                         lastName,
                         dateOfBirth,
                         countryOfResidence,
                         postalCode,
                         contactNumber,
                         token
                     }) {
        console.log(accountId)
        const requestOptions = {
            method: 'PUT',
            body: JSON.stringify({
                "id": accountId,
                "email": email,
                "firstName": firstName,
                "lastName": lastName,
                "dateOfBirth": dateOfBirth,
                "countryOfResidence": countryOfResidence,
                "postalCode": postalCode,
                "contactNumber": contactNumber
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        const user = await fetch(`${baseURL}/api/Accounts/${accountId}`, requestOptions)
            .then(async response => {
                if (!response.ok) {
                    const {error = 'An unknown error occurred'} = response.json()
                    throw new Error(error)
                }
                return response
            })
        return user
    }
}